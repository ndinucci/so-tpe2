#include <kernel.h>

int sig_two_main(void){
	bool cursor;
	mt_cons_clear();
	int signal;
	cursor = mt_cons_cursor(true);
	char buf[10];
	cprintk(LIGHTGREEN, BLUE,"Bienvenido a sig_two\n\n");
	while(true){
		printk("Ingrese la tarea a la que quiere enviar una signal\n");
		*buf=0;
		getline(buf,9);
		Task_t *task = (Task_t *) strtoul(buf, NULL, 16);
		*buf=0;
		printk("Ingrese la signal que quiere enviar a la tarea\n");
		getline(buf,3);
		signal=(int) atoi(buf);
		kill(signal,task);
		printk("Se ha intentado mandar la signal ");
		cprintk(GREEN,BLACK,"%d",signal);
		printk(" a la tarea ");
		cprintk(GREEN,BLACK,"%p\n\n",task);
		
	}
	mt_cons_clear();
	mt_cons_cursor(cursor);
}

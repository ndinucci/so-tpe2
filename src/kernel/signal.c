
#include <kernel.h>

int sys_kill(int signal, Task_t* sender, Task_t* receiver){
	Atomic();
	bool ints = SetInts(false);
	if(signal>MAX_SIG || signal<=0){
		Unatomic();	
		SetInts(ints);
		return -1;
	}
	if(sender!=receiver && (sender->priority >= receiver->priority)){
		if(receiver->signals.sig_mask[signal-1]==1){				
			receiver->signals.sig_pend[signal-1]=1;
			if(receiver->signals.paused || signal==SIG_KILL){
				Ready(receiver);
				receiver->signals.paused=false;			
			}
			Unatomic();
			SetInts(ints);
			return 0;
		}
	}
	Unatomic();
	SetInts(ints);
	return -1;
}

void sig_kill_handler(void){
	DeleteTask(mt_curr_task,1);
	return;
}

void sig_ign_handler(void){
	return;
}

int kill(int signal, Task_t* receiver){
	return sys_kill(signal, mt_curr_task, receiver);
}

void pause(void){
	Atomic();
	bool ints = SetInts(false);
	mt_curr_task->signals.paused=true;
	mt_curr_task->state = TaskSuspended;
	call_scheduler();
	SetInts(ints);
	Unatomic();	
}

sig_handler signal(int signal, sig_handler new_handler){
	bool ints = SetInts(false);
	Atomic();
	sig_handler old_handler;	
	if(signal<=2 || signal>MAX_SIG){
		SetInts(ints);
		Unatomic();	
		return NULL;	
	}
	old_handler = mt_curr_task->signals.sig_handlers[signal-1];
	mt_curr_task->signals.sig_handlers[signal-1]=new_handler;
	SetInts(ints);
	Unatomic();	
	return old_handler;	
}

void sig_check(void){
	int i;
	sig_handler aux;
	for(i=0;i<MAX_SIG;i++){
		if(mt_curr_task->signals.sig_pend[i]){	
			aux = mt_curr_task->signals.sig_handlers[i];
			mt_curr_task->signals.sig_pend[i]=0;
			if(i!=SIG_IGN-1)
				mt_curr_task->signals.sig_handlers[i]=sig_kill_handler;
			aux();		
		}	
	}
}

int setsigmask(int mask){
	int i, oldmask,aux;
	if((mask&2) != 0 || (mask&1) != 0){
		return 1;
	}
	Atomic();
	oldmask=0;
	aux=1;
	for(i=0 ; i<MAX_SIG ; i++){
		oldmask+=(mt_curr_task->signals.sig_mask[i] * (-1)+1)*aux;
		mt_curr_task->signals.sig_mask[i]=(mask%2)*(-1)+1;
		mask=mask/2;
		aux=aux*2;			
	}
	Unatomic();
	return oldmask;
}

int getsigmask(){
	int mask=0;
	int aux=1;
	int i;
	for(i=0 ; i<MAX_SIG ; i++){
		mask+=(mt_curr_task->signals.sig_mask[i]*(-1)+1)*aux;
		aux=aux*2;			
	}
	return mask;
}

void signalreset(){
	int i;
	Atomic();
	for(i=0;i<MAX_SIG;i++){
		mt_curr_task->signals.sig_handlers[i]=sig_kill_handler;	
	}
	mt_curr_task->signals.sig_handlers[1]=sig_ign_handler;
	Unatomic();
}



